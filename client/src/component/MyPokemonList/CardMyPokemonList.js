import React from "react";
import Swal from "sweetalert2";
import {
  card,
  cardsContainer,
  cardsContent,
  cardTitle,
} from "../../css/MyPokemonList/CardMyPokemonListCSS";
import { deleteMyPokemon } from "../../store/action/myPokemonAction";
import { useDispatch } from "react-redux";
import alertImage from "../../asset/sadface.png";
import goodByeImage from "../../asset/goodbye.png";
const CardPokemonList = ({ data }) => {
  const dispatch = useDispatch();
  const deletePokemon = () => {
    Swal.fire({
      title: "Are you sure want to release this pokemon?",
      imageUrl: alertImage,
      imageHeight: 150,
      imageWidth: 150,
      imageAlt: "Sad Face",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Release Me",
    }).then((result) => {
      if (result.isConfirmed) {
        dispatch(deleteMyPokemon(data.nickname));
        Swal.fire({
          imageUrl: goodByeImage,
          imageAlt: "Sad Face",
          title: `Good Bye ${data.nickname}`,
        });
      }
    });
  };

  return (
    <div className={cardsContainer()}>
      <div className={card(data.imageUrl)}>
        <div className={cardsContent()}>
          <h2 className={cardTitle()}>{data.nickname}</h2>
          <div onClick={() => deletePokemon()}>
            <p>Release Me</p>
          </div>
        </div>
      </div>
    </div>
  );
};

export default CardPokemonList;
