import React from "react";
import catchMe from "../../asset/catchme.png";
import HappyFaceImage from "../../asset/happyface.png";
import UnluckyFaceImage from "../../asset/unlucky.png";
import Swal from "sweetalert2";
import { useDispatch } from "react-redux";
import { useParams } from "react-router-dom";
// import { useDispatch } from "react-redux";
// import { useParams } from "react-router-dom";
import {
  catchPokemonStyle,
  imgCatchMeStyle,
  detailsContainer,
  leftItemStyle,
  titleStyle,
  verticalLine,
  rightItemStyle,
  abilitesStyle,
  hrStyle,
  abilitesItemsStyle,
  typesItemStyle,
  typesStyle,
  movesContainerStyle,
  movesItemStyle,
  movesStyle,
  imageStyle,
} from "../../css/PokemonDetail/DetailCSS";
const MainDetail = ({ data }) => {
  const dispatch = useDispatch();
  const param = useParams();
  const imageUrl = `https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/official-artwork/${data.id}.png`;
  const catchPokemon = () => {
    let randomNumber = Math.floor(Math.random() * 100);
    if (randomNumber < 50) {
      Swal.fire({
        imageUrl: HappyFaceImage,
        imageAlt: "Happy Face",
        title: "Success Catch Pokemon!",
        text: "Lets give him a name!",
        input: "text",
        confirmButtonText: "Save",
        showLoaderOnConfirm: true,
        preConfirm: (nickname) => {
          return fetch(`https://sheltered-beach-65265.herokuapp.com/pokemon`, {
            method: "POST",
            headers: {
              "Content-Type": "application/json",
              Accept: "application/json",
            },
            body: JSON.stringify({
              imageUrl: `https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/official-artwork/${data.id}.png`,
              name: param.pokemon,
              nickname,
            }),
          })
            .then((response) => {
              if (!response.ok) {
                throw response.statusText;
              } else {
                return response.json();
              }
            })
            .catch((error) => {
              console.log(error);
              if (nickname.length === 0) {
                Swal.showValidationMessage(
                  `Request failed: pokemon name cannot empty!`
                );
              } else if (nickname.length > 10) {
                Swal.showValidationMessage(
                  `Request failed: maximum 10 characters!`
                );
              } else if (error === "Bad Request") {
                Swal.showValidationMessage(
                  `Request failed: this pokemon name had been registered before!`
                );
              }
            });
        },
        allowOutsideClick: () => !Swal.isLoading(),
      }).then((result) => {
        if (result.isConfirmed) {
          dispatch({
            type: "ADD_MY_POKEMON",
            payload: result.value,
          });
          Swal.fire({
            imageUrl: HappyFaceImage,
            imageAlt: "Happy Face",
            text: "Success add new pokemon!",
          });
        }
      });
    } else {
      Swal.fire({
        imageUrl: UnluckyFaceImage,
        imageAlt: "Unlucky",
        text: "Failed to catch pokemon!",
        confirmButtonText: "Don't give up!",
      });
    }
  };
  return (
    <div style={{ marginTop: "80px" }}>
      <div className={catchPokemonStyle()} onClick={() => catchPokemon()}>
        <img src={catchMe} className={imgCatchMeStyle()} alt="catch-icon" />
      </div>
      <div className={detailsContainer()}>
        <div className={leftItemStyle()}>
          <img className={imageStyle()} src={imageUrl} alt="pokemon-img" />
          <h1 className={titleStyle()}>{data.name}</h1>
          <div className={typesStyle()}>
            {data.types.map(({ type }, idx) => (
              <div className={typesItemStyle(type.name)} key={idx}>
                <p>{type.name}</p>
              </div>
            ))}
          </div>
        </div>
        <div className={verticalLine()}></div>
        <div className={rightItemStyle()}>
          <h1 className={titleStyle()}>Abilities</h1>
          <div className={abilitesStyle()}>
            {data.abilities.map((abb, idx) => (
              <div className={abilitesItemsStyle()} key={idx}>
                <p>{abb.ability.name}</p>
              </div>
            ))}
          </div>

          <hr className={hrStyle()} />
          <h1 className={titleStyle()}>Moves</h1>
          <div className={movesStyle()}>
            {data.moves.map(({ move }, idx) => (
              <div className={movesContainerStyle()} key={idx}>
                <div className={movesItemStyle()}>
                  <p>{move.name}</p>
                </div>
              </div>
            ))}
          </div>
        </div>
      </div>
    </div>
  );
};

export default MainDetail;
