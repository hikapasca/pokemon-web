import React from "react";
import {
  card,
  cardsContainer,
  cardsContent,
  cardTitle,
} from "../../css/PokemonList/CardPokemonListCSS";

import { useNavigate } from "react-router-dom";
const CardPokemonList = ({ data }) => {
  const navigate = useNavigate();
  const moveToDetail = (name) => {
    navigate(`/detail/${name}`, { replace: true });
  };
  return (
    <div className={cardsContainer()} onClick={() => moveToDetail(data.name)}>
      <div className={card(data.artwork)}>
        <div className={cardsContent()}>
          <p>Owned: {data.count}</p>
          <h2 className={cardTitle()}>{data.name}</h2>
        </div>
      </div>
    </div>
  );
};

export default CardPokemonList;
