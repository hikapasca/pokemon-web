import React from "react";
import { loadingComponent } from "../../css/Other/Loading";

const Loading = () => {
  return (
    <div>
      <div className={loadingComponent()}>
        Loading
        <span></span>
      </div>
    </div>
  );
};

export default Loading;
