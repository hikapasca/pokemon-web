import React from "react";
import { emptyPokemonContainer } from "../../css/Other/EmptyPokemonCSS";
const emptyPokemon = () => {
  return (
    <div className={emptyPokemonContainer()}>
      <h1>OOPS!</h1>
      <p>You Have no Pokemon!</p>
      <p>
        <a href="/">Go To Pokemon List</a>
      </p>
    </div>
  );
};

export default emptyPokemon;
