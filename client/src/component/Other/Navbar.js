import pokemonLogo from "../../asset/pokemon-logo.png";
import pokemonListIcon from "../../asset/icon-pokemonlist.png";
import myPokemonIcon from "../../asset/icon-mypokemon.png";
import React from "react";

import { NavLink, useNavigate } from "react-router-dom";
import {
  navbarContainer,
  navbarPokemonLogo,
  liStyle,
  liItemStyle,
  aItemStyle,
} from "../../css/Other/Navbar";

const NavbarComponent = () => {
  const navigate = useNavigate();

  const moveToPokemonList = () => {
    navigate("/", { replace: true });
  };

  const moveToMyPokemon = () => {
    navigate("/mypokemon", { replace: true });
  };

  return (
    <nav>
      <ul className={navbarContainer()}>
        <li className={navbarPokemonLogo()}>
          <NavLink to="/">
            <img src={pokemonLogo} alt="pokemonLogo" />
          </NavLink>
        </li>

        <li className={liStyle()}>
          <div className={liItemStyle()} onClick={() => moveToMyPokemon()}>
            <img src={myPokemonIcon} alt="myPokemonIcon" />
            <br />
            <p className={aItemStyle()} style={{ margin: "0" }}>
              My Pokemon
            </p>
          </div>
        </li>
        <li className={liStyle()}>
          <div className={liItemStyle()} onClick={() => moveToPokemonList()}>
            <img src={pokemonListIcon} alt="pokemonListIcon" />
            <br />
            <p className={aItemStyle()} style={{ margin: "0" }}>
              Pokemon List
            </p>
          </div>
        </li>
      </ul>
    </nav>
  );
};

export default NavbarComponent;
