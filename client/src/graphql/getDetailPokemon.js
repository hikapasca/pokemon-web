import { gql } from "@apollo/client";

const GET_DETAIL_POKEMON = gql`
  query pokemon($name: String!) {
    pokemon(name: $name) {
      id
      name
      abilities {
        ability {
          name
        }
      }
      moves {
        move {
          name
          url
        }
      }
      types {
        type {
          name
        }
      }
      message
      status
      weight
      species {
        url
        name
      }
      game_indices {
        game_index
      }
      height
      location_area_encounters
      base_experience
      forms {
        url
        name
      }
      held_items {
        item {
          url
          name
        }
        version_details {
          rarity
          version {
            name
            url
          }
        }
      }
      order
    }
  }
`;

export default GET_DETAIL_POKEMON;
