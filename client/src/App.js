import { PokemonList, PokemonDetail, MyPokemonList } from "./pages";
import "./App.css";
import {
  BrowserRouter as Router,
  Route,
  Routes,
  Navigate,
} from "react-router-dom";
import store from "./store";
import { Provider } from "react-redux";
import Navbar from "./component/Other/Navbar";
import { ApolloClient, InMemoryCache, ApolloProvider } from "@apollo/client";
const client = new ApolloClient({
  uri: "https://graphql-pokeapi.vercel.app/api/graphql",
  cache: new InMemoryCache(),
});
function App() {
  return (
    <ApolloProvider client={client}>
      <Provider store={store}>
        <Router>
          <Navbar />
          <div className="App">
            <Routes>
              <Route path="/" element={<PokemonList />}></Route>
              <Route
                path="/detail/:pokemon"
                element={<PokemonDetail />}
              ></Route>
              <Route path="/mypokemon" element={<MyPokemonList />}></Route>
              <Route path="*" element={<Navigate to="/" />} />
            </Routes>
          </div>
        </Router>
      </Provider>
    </ApolloProvider>
  );
}

export default App;
