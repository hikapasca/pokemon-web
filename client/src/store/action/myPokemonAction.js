export const getMyPokemon = (setLoadingStatus) => {
  return (dispatch, getState) => {
    fetch(`https://sheltered-beach-65265.herokuapp.com/pokemon`)
      .then((resp) => {
        if (resp.ok) {
          return resp.json();
        } else {
          throw resp;
        }
      })
      .then((result) => {
        dispatch({
          type: "SET_MY_POKEMON",
          payload: result,
        });
        setLoadingStatus(false);
      })
      .catch((err) => {
        console.log("error Message:", err);
      });
  };
};

export const getOwnedPokemon = (setLoadingStatus) => {
  return (dispatch, getState) => {
    fetch(`https://sheltered-beach-65265.herokuapp.com/pokemon`)
      .then((resp) => {
        if (resp.ok) {
          return resp.json();
        } else {
          throw resp;
        }
      })
      .then((result) => {
        dispatch({
          type: "SET_MY_POKEMON",
          payload: result,
        });
        setLoadingStatus(false);
      })
      .catch((err) => {
        console.log("error Message:", err);
      });
  };
};

export const deleteMyPokemon = (pokemonId) => {
  return (dispatch, getState) => {
    fetch(`https://sheltered-beach-65265.herokuapp.com/pokemon/${pokemonId}`, {
      method: "DELETE",
      headers: {
        "Content-Type": "application/json",
        Accept: "application/json",
      },
    })
      .then((resp) => {
        if (resp.ok) {
          return resp.json();
        } else {
          throw resp;
        }
      })
      .then((result) => {
        console.log(result, "<<<<<<<<");
        dispatch({
          type: "DELETE_MY_POKEMON",
          payload: pokemonId,
        });
      })
      .catch((err) => {
        console.log("error Message:", err);
      });
  };
};
