import React from "react";
import { useParams } from "react-router-dom";
import { useQuery } from "@apollo/client";
import GET_DETAIL_POKEMON from "../graphql/getDetailPokemon";
import LoadingComponent from "../component/Other/Loading";
import DetailComponent from "../component/PokemonDetail/MainDetail";
const PokemonDetail = () => {
  const params = useParams();
  const { error, loading, data } = useQuery(GET_DETAIL_POKEMON, {
    variables: { name: params.pokemon },
  });
  if (error) {
    return `Error Message ${error.message}`;
  }
  if (loading) {
    return (
      <div>
        <LoadingComponent />
      </div>
    );
  }
  if (data) {
    return (
      <div>
        <DetailComponent data={data.pokemon} />
      </div>
    );
  }
};

export default PokemonDetail;
