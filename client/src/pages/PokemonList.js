import React, { useEffect, useState } from "react";
import { useQuery } from "@apollo/client";
import GET_POKEMONS from "../graphql/getPokemon";
import { useDispatch, useSelector } from "react-redux";
import LoadingComponent from "../component/Other/Loading";
import CardPokemonList from "../component/PokemonList/CardPokemonList";
import { cardsContainer } from "../css/PokemonList/PokemonListCSS";
import { getOwnedPokemon } from "../store/action/myPokemonAction";
const PokemonList = () => {
  const [loadingStatus, setLoadingStatus] = useState(false);
  const dispatch = useDispatch();
  const dataOwned = useSelector((state) => state.myPokemonReducer.myPokemon);
  useEffect(() => {
    setLoadingStatus(true);
    dispatch(getOwnedPokemon(setLoadingStatus));
  }, [dispatch]);
  const gqlVariables = {
    limit: 18,
    offset: 1,
  };
  const { loading, error, data } = useQuery(GET_POKEMONS, {
    variables: gqlVariables,
  });
  const count = () => {
    if (data) {
      let dataPokemon = data.pokemons.results;
      let copyData = JSON.parse(JSON.stringify(dataPokemon));
      if (dataOwned) {
        for (let i = 0; i < copyData.length; i++) {
          copyData[i].count = 0;
        }
        for (let i = 0; i < copyData.length; i++) {
          for (let j = 0; j < dataOwned.length; j++) {
            if (copyData[i].name === dataOwned[j].name) {
              copyData[i].count += 1;
            }
          }
        }
        return copyData;
      }
    }
  };
  if (loading)
    return (
      <div>
        <LoadingComponent />
      </div>
    );
  if (error) return `Error! ${error.message}`;

  if (data) {
    return (
      <div className={cardsContainer()}>
        {count().map((res, idx) => (
          <div key={idx}>
            <CardPokemonList data={res} />
          </div>
        ))}
      </div>
    );
  }
};

export default PokemonList;
