import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { getMyPokemon } from "../store/action/myPokemonAction";
import LoadingComponent from "../component/Other/Loading";
import EmptyMyPokemon from "../component/Other/EmptyPokemon";
import CardMyPokemonList from "../component/MyPokemonList/CardMyPokemonList";

import { cardsContainer } from "../css/MyPokemonList/MyPokemonListCSS";
const MyPokemonList = () => {
  const [loadingStatus, setLoadingStatus] = useState(false);
  const dispatch = useDispatch();

  const data = useSelector((state) => state.myPokemonReducer.myPokemon);
  useEffect(() => {
    setLoadingStatus(true);
    dispatch(getMyPokemon(setLoadingStatus));
  }, [dispatch]);

  if (data.length === 0 && loadingStatus === false) {
    return (
      <div>
        <EmptyMyPokemon />
      </div>
    );
  }
  if (data.length === 0 && loadingStatus === true) {
    return (
      <div>
        <LoadingComponent />
      </div>
    );
  }
  if (data) {
    return (
      <div className={cardsContainer()}>
        {data.map((res, idx) => (
          <div key={idx}>
            <CardMyPokemonList data={res} />
          </div>
        ))}
      </div>
    );
  }
};

export default MyPokemonList;
