import { css, keyframes } from "@emotion/css";

export const loadingComponent = () => {
  return css({
    position: "absolute",
    top: "50%",
    left: "50%",
    transform: "translate(-50%,-50%)",
    width: "100px",
    height: "100px",
    background: "transparent",
    border: "3px solid #3c3c3c",
    borderRadius: "50%",
    textAlign: "center",
    lineHeight: "100px",
    fontFamily: "sans-serif",
    fontSize: "14px",
    color: "black",
    letterSpacing: "4px",
    textTransform: "uppercase",
    textShadow: "0 0 10px black",
    boxShadow: "0 0 20px rgba(0,0,0,.5)",
    ":before": {
      content: `''`,
      position: "absolute",
      top: "-3px",
      left: "-3px",
      width: "100%",
      height: "100%",
      border: "3px solid transparent",
      borderTop: "3px solid red",
      borderRight: "3px solid red",
      borderRadius: "50%",
      animation: `${animateC} 2s linear infinite`,
    },
    span: {
      display: "block",
      position: "absolute",
      top: "calc(50% - 2px)",
      left: "50%",
      width: "50%",
      height: "4px",
      background: "transparent",
      transformOrigin: "left",
      animation: `${animate} 2s linear infinite`,
      ":before": {
        content: "''",
        position: "absolute",
        width: "20px",
        height: "20px",
        borderRadius: "50%",
        background: "red",
        top: "-6px",
        right: "-8px",
        boxShadow: "0 0 20px red",
      },
    },
  });
};

const animateC = keyframes({
  "0%": {
    transform: "rotate(0deg)",
  },
  "100%": {
    transform: "rotate(360deg)",
  },
});

const animate = keyframes({
  "0%": {
    transform: "rotate(45deg)",
  },
  "100%": {
    transform: "rotate(405deg)",
  },
});
