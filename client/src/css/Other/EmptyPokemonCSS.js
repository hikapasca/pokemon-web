import { css } from "@emotion/css";

export const emptyPokemonContainer = () => {
  return css({
    position: "absolute",
    top: "50%",
    left: "50%",
    transform: "translate(-50%,-50%)",
    h1: {
      fontSize: "200px",
      fontWeight: "lighter",
      textAlign: "center",
      "@media (max-width: 580px)": {
        fontSize: "150px",
      },
      "@media (max-width: 480px)": {
        fontSize: "120px",
      },
      "@media (max-width: 370px)": {
        fontSize: "90px",
      },
    },
    p: {
      textAlign: "center",
      fontSize: "30px",
      transform: "translateY(-422%)",
      "@media (max-width: 580px)": {
        fontSize: "25px",
      },
      "@media (max-width: 480px)": {
        fontSize: "20px",
      },
      "@media (max-width: 370px)": {
        fontSize: "15px",
      },
      a: {
        textDecoration: "none",
        background: "linear-gradient(rgba(0, 0, 0, 0.6), rgba(0, 0, 0, 0.3))",
        boxShadow: "grey 0px 0px 5px 0px",
        "&:hover": {
          background: "linear-gradient(rgba(0, 0, 0, 0.5), rgba(0, 0, 0, 0.5))",
          boxShadow: "grey 0px 0px 10px 0px",
        },
        padding: "12px",
        color: "black",
        fontSize: "20px",
        "@media (max-width: 580px)": {
          fontSize: "15px",
        },
        "@media (max-width: 480px)": {
          fontSize: "12px",
        },
        "@media (max-width: 370px)": {
          fontSize: "10px",
        },
      },
    },
  });
};
