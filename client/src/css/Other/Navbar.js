import { css } from "@emotion/css";
export const liStyle = () => {
  return css({
    float: "right",
    cursor: "pointer",
    width: "200px",
    borderLeft: "solid",
    borderWidth: "1px",
    "@media (max-width: 580px)": {
      borderRight: "solid",
      borderWidth: "1px",
    },
  });
};

export const navbarPokemonLogo = () => {
  return css({
    float: "left",
    padding: "12px",
    height: "20px",
    cursor: "pointer",
    "@media (max-width: 580px)": {
      display: "none",
    },
  });
};

export const navbarContainer = () => {
  return css({
    listStyleType: "none",
    marginTop: "0",
    padding: "0",
    top: "0",
    position: "fixed",
    color: "black",
    zIndex: "100001 !important",
    backgroundColor: "white",
    borderStyle: "none",
    width: "100%",
    boxShadow: "grey 0px 0px 7px 0px",
    "@media (max-width: 580px)": {
      display: "flex",
      justifyContent: "center",
    },
  });
};

export const liItemStyle = () => {
  return css({
    display: "block",
    color: "black",
    textAlign: "center",
    textWeight: "bold",
    padding: "10px 16px",
    textDecoration: "none",
    "@media (max-width: 256px)": {
      fontSize: "14px",
    },
    "@media (max-width: 232px)": {
      fontSize: "12px",
    },
    "@media (max-width: 209px)": {
      fontSize: "10px",
      padding: "7px 10px",
    },
    "@media (max-width: 163px)": {
      fontSize: "10px",
      padding: "7px 10px",
    },
    "&:hover": {
      backgroundColor: "#efefef",
    },
  });
};

export const aItemStyle = () => {
  return css`
    color: black;
    text-decoration: none;
  `;
};
