import { css, keyframes } from "@emotion/css";
export const detailsContainer = () => {
  return css({
    display: "flex",
    justifiyContent: "center",
    borderSyle: "solid",
    borderWidth: "100px",
    "@media (max-width: 750px)": {
      display: "block",
    },
  });
};

export const imageStyle = () => {
  return css({
    width: "300px",
    height: "300px",
    display: "block",
    marginLeft: "auto",
    marginRight: "auto",
    "@media (max-width: 300px)": {
      width: "150px",
      height: "150px",
    },
  });
};

export const abilitesStyle = () => {
  return css({
    display: "flex",
    justifyContent: "start",
    flexFlow: "row wrap",
    "@media (max-width: 750px)": {
      justifyContent: "center",
    },
  });
};

export const abilitesItemsStyle = () => {
  return css({
    padding: "0px 5px 0px 5px",
    borderRadius: "10px",
    marginRight: "10px",
    alignItems: "center",
    width: "120px",
    textAlign: "center",
    boxShadow: "grey 0px 0px 3px 0px",
    overflow: "hidden",
    "&:hover": {
      boxShadow: "grey 0px 0px 7px 0px",
    },
  });
};

export const typesStyle = () => {
  return css({
    display: "flex",
    justifyContent: "center",
    flexFlow: "row wrap",
  });
};

const sortByType = (type) => {
  let color;
  switch (type) {
    case "fire":
      color = "red";
      break;
    case "poison":
      color = "purple";
      break;
    case "grass":
      color = "#006400";
      break;
    case "water":
      color = "blue";
      break;
    case "bug":
      color = "#7FFF00";
      break;
    case "flying":
      color = "#00FFFF";
      break;
    case "normal":
      color = "white";
      break;

    default:
      color = "transparent";
      break;
  }
  return color;
};

export const typesItemStyle = (types) => {
  return css({
    padding: "0px 5px 0px 5px",
    borderRadius: "10px",
    marginRight: "10px",
    alignItems: "center",
    width: "120px",
    backgroundColor: `${sortByType(types)}`,
    textAlign: "center",
    boxShadow: `${sortByType(types)} 0px 0px 3px 0px`,
    overflow: "hidden",
    "&:hover": {
      boxShadow: `${sortByType(types)} 0px 0px 7px 0px`,
    },
  });
};
export const movesStyle = () => {
  return css({
    display: "flex",
    justifyContent: "start",
    flexFlow: "row wrap",
    "@media (max-width: 750px)": {
      justifyContent: "center",
    },
  });
};

export const movesItemStyle = () => {
  return css({
    padding: "0px 5px 0px 5px",
    borderRadius: "10px",
    marginRight: "5px",
    alignItems: "center",
    width: "120px",
    textAlign: "center",
    boxShadow: "grey 0px 0px 3px 0px",
    overflow: "hidden",
    "&:hover": {
      boxShadow: "grey 0px 0px 7px 0px",
    },
  });
};

export const movesContainerStyle = () => {
  return css({
    height: "auto",
    display: "flex",
    margin: "5px",
  });
};
export const leftItemStyle = () => {
  return css({
    marginLeft: "80px",
    marginRight: "80px",
    "@media (max-width: 475px)": {
      marginLeft: "40px",
      marginRight: "40px",
    },
    "@media (max-width: 380px)": {
      marginLeft: "0px",
      marginRight: "0px",
    },
  });
};

export const rightItemStyle = () => {
  return css({
    textAlign: "left",
    "@media (max-width: 750px)": {
      padding: "30px",
    },
  });
};

export const titleStyle = () => {
  return css({
    fontSize: "40px",
    textAlign: "center",
    "@media (max-width: 360px)": {
      fontSize: "30px",
    },
  });
};

export const hrStyle = () => {
  return css({
    width: "100%",
    position: "relative",
    marginTop: "30px",
  });
};

export const verticalLine = () => {
  return css`
    border-left: solid grey;
    border-width: 1px;
    margin-right: 20px;
    margin-left: 10px;
  `;
};

export const catchPokemonStyle = () => {
  return css({
    position: "fixed",
    bottom: "0",
    right: "320px",
    cursor: "pointer",
    padding: "5px",
    alignItems: "center",
    overflow: "hidden",
    animation: `${animate} 1s linear infinite`,
    "@media (max-width: 750px)": {
      right: "20px",
    },
  });
};

export const imgCatchMeStyle = () => {
  return css({
    width: "80px",
    height: "80px",
    "@media (max-width: 480px)": {
      width: "50px",
      height: "50px",
    },
  });
};

const animate = keyframes({
  "0%, 100%": {
    transform: "translateY(0%)",
  },
  "50%": {
    transform: "translateY(-30%)",
  },
});
