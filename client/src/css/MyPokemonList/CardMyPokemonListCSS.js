import { css } from "@emotion/css";
export const cardsContainer = () => {
  return css({
    margin: "10px",
  });
};

export const cardsContent = () => {
  return css({
    color: "black",
    transform: "translateY(95%)",
    borderRadius: "0 0 1rem 1rem",
    padding: "0 10px 1px 10px",
    background: "linear-gradient(rgba(0, 0, 0, 0), rgba(0, 0, 0, 0.3))",
    "@media (max-width: 420px)": {
      transform: "translateY(142%)",
      fontSize: "11px",
    },
    "@media (max-width: 300px)": {
      transform: "translateY(99%)",
      fontSize: "10px",
    },
    div: {
      p: {
        transform: "translateY(25%)",
        cursor: "pointer",
        textAlign: "center",
        background:
          "linear-gradient(rgba(222, 27, 92, 1), rgba(222, 27, 92, 0.8))",
        boxShadow: "rgba(222, 27, 92, 1) 0px 0px 5px 0px",
        "&:hover": {
          background:
            "linear-gradient(rgba(242, 5, 84, 1), rgba(242, 5, 84, 0.8))",
          boxShadow: "rgba(242, 5, 84, 1) 0px 0px 10px 0px",
        },
        borderRadius: "5px",
        padding: "7px",
        color: "black",
        fontSize: "15px",
        "@media (max-width: 420px)": {
          padding: "6px",
          fontSize: "10px",
        },
        "@media (max-width: 300px)": {
          padding: "6px",
          fontSize: "10px",
        },
      },
    },
  });
};
export const cardTitle = () => {
  return css({
    textAlign: "center",
    width: "100%",
    margin: "0",
    transform: "translateY(-580%)",
    "@media (max-width: 420px)": {
      transform: "translateY(-885%)",
    },
    "@media (max-width: 300px)": {
      transform: "translateY(-815%)",
    },
  });
};

export const card = (artworkPokemon) => {
  return css({
    backgroundColor: "white",
    backgroundSize: "80% 80%",
    color: "white",
    backgroundImage: `url(
        ${artworkPokemon})`,
    backgroundRepeat: "no-repeat",
    padding: "5rem 0 0 0",
    height: "175px",
    width: "175px",
    backgroundPosition: "center",
    borderRadius: "1rem",
    // overflow: "hidden",
    transition: "transform 500ms ease",
    boxShadow: "grey 0px 0px 3px 0px",
    "&:hover": {
      zIndex: "100000 !important",
      transform: "scale(1.01)",
      boxShadow: "grey 0px 0px 7px 0px",
    },
    "@media (max-width: 420px)": {
      height: "150px",
      width: "150px",
    },
    "@media (max-width: 300px)": {
      height: "120px",
      width: "120px",
    },
  });
};
