import { css } from "@emotion/css";
export const cardsContainer = () => {
  return css({
    margin: "10px",
  });
};

export const cardsContent = () => {
  return css({
    color: "black",
    transform: "translateY(122%)",
    borderRadius: "0 0 1rem 1rem",
    textAlign: "center",
    alignItems: "center",
    padding: "0 10px 10px 10px",
    background: "linear-gradient(rgba(0, 0, 0, 0), rgba(0, 0, 0, 0.3))",
    "@media (max-width: 420px)": {
      transform: "translateY(173%)",
      fontSize: "11px",
    },
    "@media (max-width: 300px)": {
      transform: "translateY(130%)",
      fontSize: "10px",
    },
    p: {
      transform: "translateY(-980%)",
      "@media (max-width: 420px)": {
        transform: "translateY(-1330%)",
      },
    },
  });
};
export const cardTitle = () => {
  return css({
    position: "relative",
    textAlign: "center",
    margin: "0",
  });
};

export const card = (artworkPokemon) => {
  return css({
    backgroundColor: "white",
    backgroundSize: "80% 80%",
    color: "white",
    cursor: "pointer",
    backgroundImage: `url(
        ${artworkPokemon})`,
    backgroundRepeat: "no-repeat",
    padding: "5rem 0 0 0",
    height: "175px",
    width: "175px",
    backgroundPosition: "center",
    borderRadius: "1rem",
    // overflow: "hidden",
    transition: "transform 500ms ease",
    boxShadow: "grey 0px 0px 3px 0px",
    "&:hover": {
      zIndex: "100000 !important",
      transform: "scale(1.01)",
      boxShadow: "grey 0px 0px 7px 0px",
    },
    "@media (max-width: 420px)": {
      height: "150px",
      width: "150px",
    },
    "@media (max-width: 300px)": {
      height: "120px",
      width: "120px",
    },
  });
};
