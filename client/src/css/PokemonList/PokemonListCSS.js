import { css } from "@emotion/css";
export const cardsContainer = () => {
  return css({
    display: "flex",
    justifyContent: "center",
    flexFlow: "row wrap",
    marginTop: "80px",
  });
};
