const { db } = require("../config/firestore");
const pokemonCollection = db.collection("Pokemon");

class PokemonModel {
  static getAll() {
    return pokemonCollection.get();
  }

  static getOne(id) {
    return pokemonCollection.doc(id).get();
  }

  static addPokemon(data, name) {
    return pokemonCollection.doc(name).set(data);
  }

  static deletePokemon(name) {
    return pokemonCollection.doc(name).delete();
  }
}

module.exports = PokemonModel;
