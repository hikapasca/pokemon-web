const route = require("express").Router();
const PokemonController = require("../controller/PokemonController");

route.get("/pokemon", PokemonController.getData);
route.post("/pokemon", PokemonController.addData);
route.get("/pokemon/:id", PokemonController.getOneData);
route.delete("/pokemon/:id", PokemonController.deleteData);

module.exports = route;
