const request = require("supertest");
const app = require("../app");
const { db } = require("../config/firestore");
const nickname = "testpikaka";
describe("Pokemon Test", () => {
  beforeAll(async () => {
    try {
      const time = Date.now();
      const insertData = {
        nickname,
        timeInNumber: time,
      };

      await db.collection("Pokemon").doc(nickname).set(insertData);
    } catch (err) {
      console.log(err);
    }
  });
  afterAll(async () => {
    await db.collection("Pokemon").doc(nickname).delete();
  });

  describe("GET /pokemon", () => {
    test("200 sucess read data pokemon", async (done) => {
      try {
        const response = await request(app).get(`/pokemon`);
        const { body, status } = response;
        expect(status).toBe(200);
        expect(body).toEqual(expect.any(Array));
        done();
      } catch (err) {
        done(err);
      }
    });
  });
  describe("GET /pokemon/:id", () => {
    test("200 sucess read one data pokemon", async (done) => {
      try {
        const response = await request(app).get(`/pokemon/${nickname}`);
        const { body, status } = response;
        expect(status).toBe(200);
        expect(body).toEqual(expect.any(Object));
        done();
      } catch (err) {
        done(err);
      }
    });
    test("404 failed read one data pokemon", async (done) => {
      try {
        const response = await request(app).get(`/pokemon/randomPokemon`);

        const { body, status } = response;
        expect(status).toBe(404);
        expect(body).toHaveProperty("errorMessage", "data not found");
        done();
      } catch (err) {
        done(err);
      }
    });
  });
  describe("POST /pokemon", () => {
    afterEach(async () => {
      await db.collection("Pokemon").doc("electoc").delete();
    });

    test("201 success add new data pokemon", async (done) => {
      try {
        const newNickname = "Electoc";
        const newData = {
          timeInNumber: Date.now(),
          nickname: newNickname.toLowerCase(),
        };
        const response = await request(app).post(`/pokemon/`).send(newData);

        const { body, status } = response;
        expect(status).toBe(201);
        expect(body).toEqual(expect.any(Object));
        done();
      } catch (err) {
        done(err);
      }
    });

    test("400 failed add new data pokemon", async (done) => {
      try {
        const newNickname = "";
        const newData = {
          timeInNumber: Date.now(),
          nickname: newNickname,
        };
        const response = await request(app).post(`/pokemon/`).send(newData);

        const { body, status } = response;
        expect(status).toBe(400);
        expect(body).toHaveProperty("errorMessage", "nickname cannot empty");
        done();
      } catch (err) {
        done(err);
      }
    });

    test("400 failed add new data pokemon - maximum 10 characters", async (done) => {
      try {
        const newNickname = "pokepokepoke";
        const newData = {
          timeInNumber: Date.now(),
          nickname: newNickname,
        };
        const response = await request(app).post(`/pokemon/`).send(newData);

        const { body, status } = response;
        expect(status).toBe(400);
        expect(body).toHaveProperty(
          "errorMessage",
          "nickname length maximum 10 characters"
        );
        done();
      } catch (err) {
        done(err);
      }
    });

    test("400 failed add new data pokemon", async (done) => {
      try {
        const newNickname = "testpikaka";
        const newData = {
          timeInNumber: Date.now(),
          nickname: newNickname.toLowerCase(),
        };
        const response = await request(app).post(`/pokemon/`).send(newData);

        const { body, status } = response;
        expect(status).toBe(400);
        expect(body).toHaveProperty(
          "errorMessage",
          "this name had been used before"
        );
        done();
      } catch (err) {
        done(err);
      }
    });
  });
  describe("DELETE /pokemon", () => {
    test("200 sucess delete one data pokemon", async (done) => {
      try {
        const response = await request(app).delete(`/pokemon/${nickname}`);
        const { body, status } = response;
        expect(status).toBe(200);
        expect(body).toHaveProperty("message", "success delete Pokemon");
        done();
      } catch (err) {
        done(err);
      }
    });
    test("400 failed delete one data pokemon", async (done) => {
      try {
        const nameDelete = "random";
        const response = await request(app).delete(`/pokemon/${nameDelete}`);
        const { body, status } = response;
        expect(status).toBe(404);
        expect(body).toHaveProperty("errorMessage", "data not found");
        done();
      } catch (err) {
        done(err);
      }
    });
  });
});
