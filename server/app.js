require("dotenv").config();
const express = require("express");
const app = express();
const cors = require("cors");
const route = require("./route");
const PORT = process.env.PORT || 3000;
const errorHandler = require("./middlewares/errorHandler");

app.use(cors());
app.use(express.urlencoded({ extended: true }));
app.use(express.json());
app.use(route);
app.use(errorHandler);

if (app.get("env") !== "test") {
  app.listen(PORT, function () {
    console.log(`Now running on PORT ${PORT}`);
  });
}
module.exports = app;
