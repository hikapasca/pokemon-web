function errorHandler(err, req, res, next) {
  if (err.name === "data not found") {
    return res.status(404).json({ errorMessage: "data not found" });
  } else if (err.name === "nickname cannot empty") {
    return res.status(400).json({ errorMessage: "nickname cannot empty" });
  } else if (err.name === "nickname length maximum 10 characters") {
    return res
      .status(400)
      .json({ errorMessage: "nickname length maximum 10 characters" });
  } else if (err.name === "this name had been used before") {
    return res
      .status(400)
      .json({ errorMessage: "this name had been used before" });
  }
}

module.exports = errorHandler;
