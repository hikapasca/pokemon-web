const PokemonModel = require("../models/Pokemon");

class PokemonController {
  static async getData(req, res, next) {
    try {
      const data = await PokemonModel.getAll();
      const result = [];
      data.forEach((doc) => {
        result.push(doc.data());
      });
      result.sort((a, b) =>
        a.timeInNumber > b.timeInNumber
          ? -1
          : a.timeInNumber < b.timeInNumber
          ? 1
          : 0
      );
      return res.status(200).json(result);
    } catch (error) {}
  }

  static async getOneData(req, res, next) {
    try {
      const param = req.params.id;
      const data = await PokemonModel.getOne(param);
      if (!data.data()) {
        throw { name: "data not found" };
      } else {
        return res.status(200).json(data.data());
      }
    } catch (err) {
      next(err);
    }
  }

  static async addData(req, res, next) {
    try {
      let nickname = req.body.nickname.toLowerCase();

      const timeInNumber = Date.now();

      if (!nickname || nickname == "") {
        throw { name: "nickname cannot empty" };
      } else if (nickname.length > 10) {
        throw { name: "nickname length maximum 10 characters" };
      } else {
        const data = await PokemonModel.getOne(nickname);
        if (data.data()) {
          throw { name: "this name had been used before" };
        } else {
          req.body.nickname = req.body.nickname.toLowerCase();
          req.body.timeInNumber = timeInNumber;
          const result = await PokemonModel.addPokemon(req.body, nickname);
          return res.status(201).json(req.body);
        }
      }
    } catch (err) {
      next(err);
    }
  }

  static async deleteData(req, res, next) {
    try {
      let params = req.params.id;
      const data = await PokemonModel.getOne(params);
      if (!data.data()) {
        throw { name: "data not found" };
      } else {
        const deleted = await PokemonModel.deletePokemon(params);
        return res.status(200).json({ message: "success delete Pokemon" });
      }
    } catch (err) {
      next(err);
    }
  }
}

module.exports = PokemonController;
