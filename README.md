# pokemon

A Pokemon App.
<img src="./examples/pokemonList.png" width="720">
<br>

Website:
<a href="https://pokemon-6c98d.web.app/" >Go To Website</a >

This app has:

- Used Technology on Server Port 3000:

  - Programming Language : Node Js with Express
  - Database : Firestore
  - TDD Test : Jest, Supertest

- Used Technology on Client:
  - Programming Language : React JS
  - Orchestrator : Graph QL - Apollo Client, React-redux
  - CSS : Emotion
  - Alert: Sweetalert2
  - TDD Test : Jest, React Testing Library

## Pokemon Page

#### Main Page - Pokemon List

<img src="./examples/pokemonList.png" width="720">
<br>

#### Pokemon Detail

<img src="./examples/pokemonDetail.png" width="720">
<br>

#### My Pokemon

<img src="./examples/mypokemon.png" width="720">
<br>

#### My Pokemon - if empty

<img src="./examples/mypokemonEmpty.png" width="720">
<br>

## RESTful endpoints

### GET /pokemon

> Get all my pokemon

_URL_

```
https://sheltered-beach-65265.herokuapp.com/pokemon
```

_Request Header_

```
not needed
```

_Request Body_

```
not needed
```

_Response (200)_

```
[
    {
        "nickname": "turtle",
        "timeInNumber": 1636641336697,
        "imageUrl": "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/official-artwork/9.png",
        "name": "blastoise"
    }
]


```

### POST /pokemon

> add new my pokemon

_URL_

```
https://sheltered-beach-65265.herokuapp.com/pokemon
```

_Request Header_

```
not needed
```

_Request Body_

```
imageUrl, name, nickname
```

_Response (201)_

```
{
    "nickname": "storm",
    "timeInNumber": 1636641494599,
    "imageUrl": "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/official-artwork/9.png",
    "name": "blastoise"
}
```

_Response (400 - Bad Request)_

```
{
  "message": "nickname cannot empty, "nickname length maximum 10 characters", "this name had been used before"
}
```

### DELETE /pokemon/:id

> DELETE my pokemon.

_URL_

```
https://sheltered-beach-65265.herokuapp.com/pokemon/:id
```

_Request Body_

```
not needed
```

_Request Header_

```
not needed
```

_Response (200)_

```
{
    "message": "success delete Pokemon"
}
```

_Response (404 - Not Found)_

```
{
  "message": "data not found"
}
```
